#ifndef metShift_h
#define metShift_h

// Standard libraries

#include <TLorentzVector.h>
#include <TMath.h>
#include <ROOT/RVec.hxx>

// metShift class
class metShift {
  public:
    metShift ();
    ~metShift ();
    std::vector<double> get_shifted_met(
      ROOT::VecOps::RVec<float> object_eta,
      ROOT::VecOps::RVec<float> object_phi,
      ROOT::VecOps::RVec<float> object_initial_pt,
      ROOT::VecOps::RVec<float> object_initial_mass,
      ROOT::VecOps::RVec<float> object_final_pt,
      ROOT::VecOps::RVec<float> object_final_mass,
      float Met_pt,
      float Met_phi
    );

};

#endif // metShift_h
