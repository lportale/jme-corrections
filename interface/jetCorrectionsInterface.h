#ifndef jetCorrectionsInterface_h
#define jetCorrectionsInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class jetCorrectionsInterface                                                                                //
//                                                                                                                //
//   Class to compute JEC values from correctionlib json.                                                         //
//                                                                                                                //
//   Author: Jona Motta (jona.motta@cern.ch)                                                                      //
//   Date  : April 2024                                                                                           //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries

//CMSSW libraries
#include "Base/Modules/interface/correctionWrapper.h"
#include <ROOT/RVec.hxx>

typedef ROOT::VecOps::RVec<float> fRVec;

class jetCorrections {
  public:
    jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3, std::string L2L3);
    jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3);
    ~jetCorrections();
    
    fRVec get_jec_sf(fRVec A, fRVec eta, fRVec pt, Float_t rho, fRVec rawF);
    fRVec get_jec_sf(fRVec A, fRVec eta, fRVec phi, fRVec pt, Float_t rho, fRVec rawF);

  private:
    std::string datatype_;
    MyCorrections jecL1;
    MyCorrections jecL2;
    MyCorrections jecL3;
    MyCorrections jecL2L3;

};

#endif // jetCorrectionsInterface_h
