import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/JME/python/jetCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class jecRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.runPeriod = kwargs.pop("runPeriod")
        self.runEra = kwargs.pop("runEra")
        self.type = kwargs.pop("type")

        self.LUT = self.type+"_"+("mc" if self.isMC else "data")+str(self.year)+self.runPeriod

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += self.runPeriod
        except KeyError:
            pass

        self.corrKey = prefix+year

        if self.year < 2022:
            raise ValueError("Only implemented for Run3 datasets")

        if not os.getenv("_jercorr"):
            os.environ["_jercorr"] = "_jercorr"
            
            if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libCorrectionsJME.so")
            base = "{}/{}/src/Corrections/JME".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            ROOT.gROOT.ProcessLine(".L {}/interface/jetCorrectionsInterface.h".format(base))

        if self.isMC:
            L1 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L1"]+corrCfg[self.type][self.corrKey]["AK"]
            L2 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L2"]+corrCfg[self.type][self.corrKey]["AK"]
            L3 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsMC"]["L3"]+corrCfg[self.type][self.corrKey]["AK"]

            ROOT.gInterpreter.Declare("""
                auto %s = jetCorrections("%s", "%s", "%s", "%s");
            """ % (self.LUT,
                   corrCfg[self.type][self.corrKey]["fileName"],
                   L1, L2, L3))
        else:
            L1   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L1"]+corrCfg[self.type][self.corrKey]["AK"]
            L2   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L2"]+corrCfg[self.type][self.corrKey]["AK"]
            L3   = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L3"]+corrCfg[self.type][self.corrKey]["AK"]
            L2L3 = corrCfg[self.type][self.corrKey]["corrNameMC"]+corrCfg["tagsDATA"]["L2L3"]+corrCfg[self.type][self.corrKey]["AK"]

            ROOT.gInterpreter.Declare("""
                auto %s = jetCorrections("%s", "%s", "%s", "%s", "%s");
            """ % (self.LUT,
                   corrCfg[self.type][self.corrKey]["fileName"],
                   L1, L2, L3, L2L3))

    def run(self, df):
        branches = []

        if self.type == "jerc":
            if self.year == 2023 and self.runPeriod == "postBPix":
                df = df.Define("jec_sf", 
                               """%s.get_jec_sf(Jet_area, Jet_eta, Jet_phi, Jet_pt, 
                                    Rho_fixedGridRhoFastjetAll, Jet_rawFactor)""" % self.LUT)
            else:
                df = df.Define("jec_sf", 
                               """%s.get_jec_sf(Jet_area, Jet_eta, Jet_pt, 
                                    Rho_fixedGridRhoFastjetAll, Jet_rawFactor)""" % self.LUT)

            df = df.Define("Jet_pt_corr", 
                           "Jet_pt * (1 - Jet_rawFactor) * jec_sf")

            df = df.Define("Jet_mass_corr", 
                           "Jet_mass * (1 - Jet_rawFactor) * jec_sf")

            branches.append("jec_sf")
            branches.append("Jet_pt_corr")
            branches.append("Jet_mass_corr")

        elif self.type == "fatjerc":
            if self.year == 2023 and self.runPeriod == "postBPix":
                df = df.Define("fatjec_sf", 
                               """%s.get_jec_sf(FatJet_area, FatJet_eta, FatJet_phi, FatJet_pt, 
                                    Rho_fixedGridRhoFastjetAll, FatJet_rawFactor)""" % self.LUT)
            else:
                df = df.Define("fatjec_sf", 
                               """%s.get_jec_sf(FatJet_area, FatJet_eta, FatJet_pt, 
                                    Rho_fixedGridRhoFastjetAll, FatJet_rawFactor)""" % self.LUT)

            df = df.Define("FatJet_pt_corr", 
                           "FatJet_pt * (1 - FatJet_rawFactor) * fatjec_sf")

            df = df.Define("FatJet_mass_corr", 
                           "FatJet_mass * (1 - FatJet_rawFactor) * fatjec_sf")

            branches.append("fatjec_sf")
            branches.append("FatJet_pt_corr")
            branches.append("FatJet_mass_corr")

        else:
            raise ValueError("Only jerc/fatjerc available")

        return df, branches


def jecRDF(**kwargs):
    """
    Module to re-apply JEC corrections to AK4 and AK8 jets.
    JEC corrections are by default already included in Jet_* variables in NanoAOD but
    JME POG always suggests to re-apply them at analysis level in order to have the 
    latest and greatest conditions included.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jecProviderRDF
            path: Base.Modules.jec
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.config.runPeriod
                runEra: self.dataset.runEra
                type: jerc/fatjerc
    """

    return lambda: jecRDFProducer(**kwargs)

