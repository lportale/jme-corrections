#include "Corrections/JME/interface/PUjetID_SFULinterface.h"

// Constructor
PUjetID_SFULinterface::PUjetID_SFULinterface (int year, std::string filename):
  corr(filename, "PUJetID_eff")
{
  year = year_;
}


std::vector<double> PUjetID_SFULinterface::get_pu_weights(
  fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass, iRVec Jet_jetId, iRVec Jet_puId,
  fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
  fRVec lep_pt, fRVec lep_eta, fRVec lep_phi, fRVec lep_mass)
{
  double sf = 1.;
  double sf_up = 1.;
  double sf_down = 1.;

  std::vector <TLorentzVector> lep_tlv;
  for (size_t i = 0; i < lep_pt.size(); i++) {
    auto tmp_tlv = TLorentzVector();
    tmp_tlv.SetPtEtaPhiM(lep_pt[i], lep_eta[i], lep_phi[i], lep_mass[i]);
    lep_tlv.push_back(tmp_tlv);
  }

  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if (Jet_jetId[ijet] < 2)
      continue;
    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    if (jet_tlv.Pt() < 20 || jet_tlv.Pt() > 50 || fabs(jet_tlv.Eta()) > 4.7)
      continue;
    bool matched_with_lepton = false;
    for (auto &tlv: lep_tlv) {
      if (jet_tlv.DeltaR(tlv) < 0.5) {
        matched_with_lepton = true;
        break;
      }
    }
    if (matched_with_lepton)
      continue;

    // noisy jet removal for 2017
    if (year_ == 2017 && fabs(jet_tlv.Eta()) > 2.65 && fabs(jet_tlv.Eta()) < 3.139)
      continue;

    bool isRealJet = false;
    TLorentzVector genjet_tlv;
    for (unsigned int igj = 0; igj < GenJet_pt.size(); igj++) {
      genjet_tlv.SetPtEtaPhiM(GenJet_pt.at(igj), GenJet_eta.at(igj), GenJet_phi.at(igj), GenJet_mass.at(igj));
      if (jet_tlv.DeltaR(genjet_tlv) < 0.4) {
        isRealJet = true;
        break;
      }
    }
    if (isRealJet) {
      auto jet_sf = corr.eval({jet_tlv.Eta(), jet_tlv.Pt(), "nom", "L"});
      auto jet_sf_up = corr.eval({jet_tlv.Eta(), jet_tlv.Pt(), "up", "L"});
      auto jet_sf_down = corr.eval({jet_tlv.Eta(), jet_tlv.Pt(), "down", "L"});
      if (Jet_puId[ijet] >= 1) {  // Loose WP
        sf *= jet_sf;
        sf_up *= jet_sf_up;
        sf_down *= jet_sf_down;
      }
    }
  }

  return {sf, sf_up, sf_down};

}


// Destructor
PUjetID_SFULinterface::~PUjetID_SFULinterface() {}
