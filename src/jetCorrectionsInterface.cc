#include "Corrections/JME/interface/jetCorrectionsInterface.h"

jetCorrections::jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3, std::string L2L3) : 
  jecL1(filename, L1),
  jecL2(filename, L2),
  jecL3(filename, L3),
  jecL2L3(filename, L2L3)
{
  datatype_ = "data";
}

jetCorrections::jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3) : 
  jecL1(filename, L1),
  jecL2(filename, L2),
  jecL3(filename, L3),
  jecL2L3(filename, L3) // dummy to avoid compilation errors
{
  datatype_ = "mc";
}

// Destructor
jetCorrections::~jetCorrections() {}

fRVec jetCorrections::get_jec_sf(fRVec A, fRVec eta, fRVec pt, Float_t rho, fRVec rawF)
{
  fRVec jec_sf;
  for (size_t i = 0; i < pt.size(); i++) {
      double jetEta = eta[i];
      double ptRaw = pt[i] * (1 - rawF[i]);

      double ptL1 = pt[i] * jecL1.eval({A[i], jetEta, ptRaw, rho});
      double ptL2 = ptL1  * jecL2.eval({jetEta, ptL1});
      double ptL3 = ptL2  * jecL3.eval({jetEta, ptL2});

      if (datatype_ == "data") {
        double ptL2L3 = ptL3  * jecL2L3.eval({jetEta, ptL3});
        jec_sf.push_back(ptL2L3 / ptRaw);
      }
      else {
        jec_sf.push_back(ptL3 / ptRaw);
      }
  }
  return jec_sf;
}

fRVec jetCorrections::get_jec_sf(fRVec A, fRVec eta, fRVec phi, fRVec pt, Float_t rho, fRVec rawF)
{
  fRVec jec_sf;
  for (size_t i = 0; i < pt.size(); i++) {
      double jetEta = eta[i];
      double jetPhi = phi[i];
      double ptRaw = pt[i] * (1 - rawF[i]);

      double ptL1 = pt[i] * jecL1.eval({A[i], jetEta, ptRaw, rho});
      double ptL2 = ptL1  * jecL2.eval({jetEta, jetPhi, ptL1});
      double ptL3 = ptL2  * jecL3.eval({jetEta, ptL2});

      if (datatype_ == "data") {
        double ptL2L3 = ptL3  * jecL2L3.eval({jetEta, ptL3});
        jec_sf.push_back(ptL2L3 / ptRaw);
      }
      else {
        jec_sf.push_back(ptL3 / ptRaw);
      }
  }
  return jec_sf;
}

